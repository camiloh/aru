// Copyright 2018-2020 Camilo Higuita <milo.h@aol.com>
// Copyright 2018-2020 Nitrux Latinoamericana S.C.
//
// SPDX-License-Identifier: GPL-3.0-or-later
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QIcon>
#include <QCommandLineParser>

#ifdef Q_OS_ANDROID
#include <QGuiApplication>
#include "mauiandroid.h"
#else
#include <QApplication>
#endif

#include <KI18n/KLocalizedString>

#include <MauiKit/Core/mauiapp.h>
#include "../aru_version.h"

#define ARU_URI "org.maui.aru"

Q_DECL_EXPORT int main(int argc, char *argv[])
{
  QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
  QCoreApplication::setAttribute(Qt::AA_DontCreateNativeWidgetSiblings);
  QCoreApplication::setAttribute(Qt::AA_UseHighDpiPixmaps, true);
  QCoreApplication::setAttribute(Qt::AA_DisableSessionManager, true);

#ifdef Q_OS_ANDROID
  QGuiApplication app(argc, argv);
  if (!MAUIAndroid::checkRunTimePermissions({"android.permission.WRITE_EXTERNAL_STORAGE"}))
    return -1;
#else
  QApplication app(argc, argv);
#endif

  app.setOrganizationName(QStringLiteral("Maui"));
  app.setWindowIcon(QIcon(":/assets/aru.svg"));
  MauiApp::instance()->setIconName("qrc:/assets/aru.svg");

  KLocalizedString::setApplicationDomain("aru");
  KAboutData about(QStringLiteral("aru"), i18n("Aru"), ARU_VERSION_STRING, i18n("A border collie adventures."),
                   KAboutLicense::LGPL_V3, i18n("© 2021 Camilo Higuita"));
  about.addAuthor(i18n("Camilo Higuita"), i18n("Developer"), QStringLiteral("milo.h@aol.com"));
  about.setHomepage("https://mauikit.org");
  about.setProductName("maui/aru");
  about.setBugAddress("https://invent.kde.org/camiloh/aru/-/issues");
  about.setOrganizationDomain(ARU_URI);
  about.setProgramLogo(app.windowIcon());

  KAboutData::setApplicationData(about);

  QCommandLineParser parser;
  parser.process(app);

  about.setupCommandLine(&parser);
  about.processCommandLine(&parser);

  QQmlApplicationEngine engine;
  const QUrl url(QStringLiteral("qrc:/main.qml"));
  QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                   &app, [url](QObject *obj, const QUrl &objUrl)
  {
    if (!obj && url == objUrl)
      QCoreApplication::exit(-1);

  }, Qt::QueuedConnection);

  engine.load(url);

  return app.exec();
}

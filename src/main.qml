// Copyright 2018-2020 Camilo Higuita <milo.h@aol.com>
// Copyright 2018-2020 Nitrux Latinoamericana S.C.
//
// SPDX-License-Identifier: GPL-3.0-or-later

import QtQml 2.14
import QtQuick 2.14
import QtQuick.Controls 2.14
import QtQuick.Layouts 1.3
import QtMultimedia 5.14

import org.kde.kirigami 2.7 as Kirigami
import org.mauikit.controls 1.2 as Maui




Maui.ApplicationWindow
{
    id: root
    title:  currentTab ? currentTab.title : ""

    floatingHeader: true
    autoHideHeader: true

    headBar.farLeftContent: ToolButton
    {
        icon.name: "love"
        onClicked: about()
    }

    StackView
    {
        id: _stackView
        anchors.fill: parent
       initialItem:  Maui.Holder
        {
            visible: true
            emoji: "qrc:/assets/illus1.png"
            emojiSize: 200
            isMask: false
            title: "Aru!"
            body: "Let's start a new game."

            Action
            {
                text: i18n("Start")
                icon.name: "media-playback-start"
                onTriggered: _stackView.push(_gameView)
            }

            Action
            {
                text: i18n("Configure")
                icon.name: "settings-configure"
            }
        }

       Component
       {
           id: _gameView
           BearWhack
           {

           }
       }

    }


}
